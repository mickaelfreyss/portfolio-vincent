$(document).ready(function () {
    var neptune = $('.neptune');
    var pluto = $('.pluto');
    var uranus = $('.uranus');
    var jupiter = $('.jupiter');
    var mercury = $('.mercury');
    var luna = $('.luna');
    var saturn = $('.saturn');
    var venus = $('.venus');
    var mars = $('.mars');
    var polaris = $('.polaris');
    var sun = $('.sun');

    neptune.angle = 0;
    pluto.angle = 0;
    uranus.angle = 0;
    jupiter.angle = 0;
    mercury.angle = 0;
    luna.angle = 0;
    saturn.angle = 2;
    venus.angle = 1;
    mars.angle = 3;
    sun.angle = 4;
    polaris.angle = 5;

    function init() {
        setInterval(rotate, 10);
    }

    function rotate(){
        angleCalc(neptune, 300, 0.001);
        angleCalc(pluto, 200, -0.002);
        angleCalc(uranus, 250, -0.0013);
        angleCalc(jupiter, 350, 0.0007);
        angleCalc(mercury, 150, -0.0016);
        angleCalc(luna, 400, 0.001);
        angleCalc(saturn, 420, 0.0006);
        angleCalc(venus, 310, -0.0008);
        angleCalc(mars, 360, 0.0009);
        angleCalc(sun, 450, -0.0005);
        angleCalc(polaris, 380, 0.0007);
        updatePosition(neptune, '50', '10C6FC');
        updatePosition(pluto, '40', '835AEB');
        updatePosition(uranus, '80', '002453');
        updatePosition(jupiter, '90', 'FF2B4A');
        updatePosition(mercury, '70', 'FF2B4A');
        updatePosition(luna, '50', '002453');
        updatePosition(saturn, '100', '835AEB');
        updatePosition(venus, '85', '10C6FC');
        updatePosition(mars, '90', 'D1F2FF');
        updatePosition(sun, '110', 'FFFAED');
        updatePosition(polaris, '100', 'EAE8FC');
    }

    function angleCalc(obj, hyp, Dang){
        let xOut = Math.cos(obj.angle)*hyp;
        let yOut = Math.sin(obj.angle)*hyp;
        obj.angle += Dang;
        obj.x = Number(xOut.toFixed(2));
        obj.y = Number(yOut.toFixed(2));
    }

    function updatePosition(obj, blur, color){
        obj.css("boxShadow", obj.x.toString() +
            "px "+ obj.y.toString() +
            "px " + blur.toString() +
            "px #"+ color);
    }

    init();
});