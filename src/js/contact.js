$("#formulaire-contact").submit(function(e){
    e.preventDefault();

    var form = $('#formulaire-contact');

    // Serialize the form data.
    var formData = $(form).serialize();
    var formMessages = $('#formMessages');

    $.ajax({
        type: "POST",
        url: "/contact.php",
        data: formData,
    }).done(function(response) {

        // Make sure that the formMessages div has the 'success' class.
        $(formMessages).removeClass('alert alert-warning');
        $(formMessages).addClass('alert alert-success');

        // Set the message text.
        $(formMessages).text('Message envoyé !');

        // Clear the form.
        $('#name').val('');
        $('#email').val('');
        $('#message').val('');
    }).fail(function(data) {
        // Make sure that the formMessages div has the 'success' class.
        $(formMessages).removeClass('alert alert-success');
        $(formMessages).addClass('alert alert-warning');

        $(formMessages).text('Une erreur est survenue, merci de reessayer');

    });
});
