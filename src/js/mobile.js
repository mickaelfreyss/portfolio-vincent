$(document).ready(function () {
    $('#burger').on("click", function () {
        $('#burger, .main-nav').toggleClass('active');
    });

    $('.main-nav').find('a').on("click", function() {
        $('#burger, .main-nav').removeClass('active');
    });
});
