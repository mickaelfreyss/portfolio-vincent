import 'bootstrap';
import './mobile';
import './contact';
import './animation/planet';
import SmoothScroll from 'smooth-scroll';
import 'ekko-lightbox';

var scroll = new SmoothScroll('a[href*="#"');

$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});